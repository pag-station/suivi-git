use std::fmt::{Display, Formatter};

pub struct Spinner {
    ticks: usize,
    roll: usize,
    frames: &'static [&'static str],
    current_frame: usize,
    per_frame: usize,
}

impl Default for Spinner {
    fn default() -> Self {
        Self::new(&[ "⣇", "⣦", "⣴", "⣸", "⢹", "⠻", "⠟", "⡏" ], 100)
    }
}

impl<'a> Spinner {
    pub fn current(&self) -> &'static str {
        self.frames[self.ticks as usize]
    }

    pub fn new(frames: &'static [&'static str], per_frame: usize) -> Self {
        Spinner {
            frames,
            ticks: 0,
            roll: frames.len(),
            current_frame: 0,
            per_frame,
        }
    }

    pub fn tick(&mut self, frames: usize) {
        let up = self.current_frame + frames >= self.per_frame;
        self.ticks = if up {
            (self.ticks + 1) % self.roll
        } else {
            self.ticks
        };
        self.current_frame = if up { 0 } else { self.current_frame + frames };
    }
}

impl Display for Spinner {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.current())
    }
}

impl<'a> Clone for Spinner {
    fn clone(&self) -> Self {
        Spinner {
            ticks: self.ticks,
            roll: self.roll,
            frames: self.frames,
            current_frame: self.current_frame,
            per_frame: self.per_frame,
        }
    }
}
