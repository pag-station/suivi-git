mod spinner;

extern crate anyhow;
extern crate mysql;

use std::fmt::{Display, Formatter};
use anyhow::Result;
use mysql::*;
use mysql::prelude::*;
use std::process::Command;
use std::str::FromStr;

use std::sync::mpsc::{channel, Receiver};
use std::thread;
use std::thread::sleep;
use std::time::{Duration, SystemTime};
use crate::spinner::Spinner;

const LINE_LENGTH: usize = 52;

struct GitCommit {
    hash: String,
    date: String,
}

impl FromStr for GitCommit {
    type Err = ();

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(GitCommit {
            hash: s[..40].to_string(),
            date: s[41..].to_string(),
        }
        )
    }
}

pub struct GoLeft();

impl Display for GoLeft {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}[0G", 27 as char)
    }
}

fn main() -> Result<()> {
    let args = std::env::args().skip(1).collect::<Vec<String>>();
    if args.len() < 1 {
        eprintln!("usage: db-dsn [ log-command ]");
        std::process::exit(1);
    }
    let db_dsn = args.get(0).expect("already tested");

    let opts = Opts::from_url(&db_dsn)?;

    let pool = Pool::new(opts)?;

    let mut conn = pool.get_conn()?;

    conn.query_drop(r"
        CREATE TABLE IF NOT EXISTS GIT_COMMITS (
            hash CHAR(40) PRIMARY KEY,
            date DATE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
    ")?;

    conn.query_drop(r"
        CREATE TABLE IF NOT EXISTS GIT_FILES
        (
            hash CHAR(40),
            file VARCHAR(255),
            PRIMARY KEY (hash, file),
            CONSTRAINT `fk_hash_files` FOREIGN KEY (hash) REFERENCES GIT_COMMITS (hash)
        ) ENGINE = InnoDB
          DEFAULT CHARSET = utf8mb4
          COLLATE = utf8mb4_unicode_ci
    ")?;

    let mut command_args = vec![String::from("log"), String::from("--format=%H %as")];
    command_args.extend(args.iter().skip(1).cloned());

    println!("execute: git {}", command_args.join(" "));

    let mut cmd = Command::new("git");
    let raw_log = cmd.args(&command_args).output()?.stdout;

    let log = String::from(std::str::from_utf8(&raw_log)?);

    let number_of_hashes: usize = log.len() / LINE_LENGTH;

    println!("About to extract {} commits", number_of_hashes);
    let (tx, rx) = channel();
    thread::spawn(move || {
        let mut hashs = vec![];
        hashs.reserve(number_of_hashes.clone());
        conn.exec_batch(
            "insert into GIT_COMMITS (hash, date) values (:hash, :date) ON DUPLICATE KEY UPDATE date=date",
            log.lines().map(|p| {
                let p = p.parse::<GitCommit>().expect("extracted previously");
                let _ = tx.send(());
                hashs.push(p.hash.clone());
                params! {
                "hash" => p.hash,
                "date" => p.date,
            }
            }),
        ).expect("insert into git commits should work");

        conn.exec_batch(
            "insert into GIT_FILES (hash, file) values (:hash, :file) ON DUPLICATE KEY UPDATE hash=hash",
            hashs.iter().flat_map(|hash| {
                let mut cmd = Command::new("git");
                let raw_log = cmd.args(&["diff-tree", "--no-commit-id", "--name-only", "-r", &hash]).output().expect("commit exists").stdout;
                let _ = tx.send(());
                let filenames = String::from(std::str::from_utf8(&raw_log).expect("utf8 conversion"));
                filenames.lines().map(|line| {
                    (hash.to_string(), line.to_string())
                }).collect::<Vec<(String, String)>>()
            }
            )
                .map(|(hash, filename)| {
                    params! {
                    "hash" => hash,
                    "file" => filename,
            }
                }),
        ).expect("insert into git_files should work");
    });

    println!("Inserting into git_commits ...");

    await_results(number_of_hashes, &rx);
    println!("{}{}/{} ✔ done inserting into git_commits", GoLeft(), number_of_hashes, number_of_hashes);
    println!("Inserting into git_files ...");
    await_results(number_of_hashes, &rx);
    println!("{}{}/{} ✔ done inserting into git_files", GoLeft(), number_of_hashes, number_of_hashes);

    Ok(())
}

fn await_results(number_of_hashes: usize, rx: &Receiver<()>) {
    let mut spinner = Spinner::default();
    let mut iterations_done = 0;
    let mut past = SystemTime::now();
    loop {
        while let Some(()) = match rx.try_recv() {
            Ok(()) => Some(()),
            _ => None
        } {
            iterations_done += 1
        }

        if iterations_done == number_of_hashes {
            break;
        }

        print!("{} {}/{}", spinner, iterations_done, number_of_hashes);
        print!("{}", GoLeft());

        sleep(Duration::from_millis(5));

        let elapsed = past.elapsed();
        past = SystemTime::now();
        let elapsed = elapsed.expect("elapsed is not err");
        spinner.tick(elapsed.as_millis() as usize);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn parse_git_commit() -> Result<(), ()> {
        let line = "ajzejazdlisfjhqjshd1238917387ueiazeuoaz5 2014-04-14T16:13:23+02:00";

        let commit = line.parse::<GitCommit>()?;
        assert_eq!(commit.hash, "ajzejazdlisfjhqjshd1238917387ueiazeuoaz5");
        assert_eq!(commit.date, "2014-04-14T16:13:23+02:00");
        Ok(())
    }
}
